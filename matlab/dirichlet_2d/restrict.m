function uc = restrict(uf)
    uc = uf(1:2:end,1:2:end);
end